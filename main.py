def sum(a, b, c):
    return a + b + c


def board(xstate, zstate):
    zero = ' + ' if xstate[0] else (' O ' if zstate[0] else '   ')
    one = ' + ' if xstate[1] else (' O ' if zstate[1] else '   ')
    two = ' + ' if xstate[2] else (' O ' if zstate[2] else '   ')
    three = ' + ' if xstate[3] else (' O ' if zstate[3] else '   ')
    four = ' + ' if xstate[4] else (' O ' if zstate[4] else '   ')
    five = ' + ' if xstate[5] else (' O ' if zstate[5] else '   ')
    six = ' + ' if xstate[6] else (' O ' if zstate[6] else '   ')
    seven = ' + ' if xstate[7] else (' O ' if zstate[7] else '   ')
    eight = ' + ' if xstate[8] else (' O ' if zstate[8] else '   ')

    print("-------------")
    print("|" + zero + "|" + one + "|" + two + "|")
    print("|---|---|---|")
    print("|" + three + "|" + four + "|" + five + "|")
    print("|---|---|---|")
    print("|" + six + "|" + seven + "|" + eight + "|")
    print("-------------")


def winCheck (xstate, zstate):
    wins = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]
    for win in wins:
        if (sum(xstate[win[0]], xstate[win[1]], xstate[win[2]]) == 3):
            print ("+ won")
            return 1

        if (sum(zstate[win[0]], zstate[win[1]], zstate[win[2]]) == 3):
            print ("O won")
            return 0
    return -1


xstate = [0, 0, 0, 0, 0, 0, 0, 0, 0]
zstate = [0, 0, 0, 0, 0, 0, 0, 0, 0]
turn = 1

print("WELCOME TO ALUPLUS GAME: ")
print("-------")
print("|0|1|2|")
print("|-----|")
print("|3|4|5|")
print("|-----|")
print("|6|7|8|")
print("-------")
print("")
print("Rule of game:")
print("1. Need two player one is O and another is +.")
print("2. Take turn to fill empty box shown in above.")
print("3. Enter a number of box in which you want to fill.")
print("4. First one to fill their mark in a row or column or diagonal wins.")

while True:
    board(xstate, zstate)

    addedValues = []

    if turn == 1:
        print("X's turn.")
        print("Please enter the cell value to place your mark: ")
        value = input()
        if (value >= 9):
            print ("Enter value between 0 and 9")
            value = input()

        xstate[value] = 1
    else:
        print("O's turn.")
        print("Please enter the cell value to place your mark: ")
        value = input()

        if(value>=9):
            print ("Enter value between 0 and 9")
            value = input()

        zstate[value] = 1
    cwin = winCheck(xstate, zstate)
    if cwin != -1:
        print("Match over")
        break

    turn = 1-turn
